<?php

use App\Http\Controllers\Admin\AdviserController;
use App\Models\User;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard/users', function () {
    return Inertia::render('Users/Index', [
        'users' => User::all()
    ]);
})->name('users.index');

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::resource('/dashboard/advisers', AdviserController::class);
    Route::post('/dashboard/advisers/imports', [AdviserController::class, 'imports'])->name('advisers.imports');
    Route::resource('/dashboard/products', App\Http\Controllers\ProductController::class);
    Route::resource('/dashboard/sales', App\Http\Controllers\SaleController::class);
    Route::post('/dashboard/sales/imports', [App\Http\Controllers\SaleController::class, 'imports'])->name('sales.imports');
    Route::resource('commissions', App\Http\Controllers\CommissionController::class);
});

