<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Inertia::share('flash', function () {
            return ['status' => Session::get('status')];
        });

        // Route::prefix('api')
        //     ->middleware('api')
        //     ->as('api.')
        //     ->namespace($this->app->getNamespace() . 'Http\Controllers\API')
        //     ->group(base_path('routes/api.php'));
    }
}
