<?php

namespace App\Repositories;

use App\Models\Commission;
use App\Repositories\BaseRepository;

/**
 * Class CommissionRepository
 * @package App\Repositories
 * @version February 4, 2021, 6:59 pm UTC
 */

class CommissionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'level0',
        'level1',
        'level2',
        'level3',
        'level4',
        'level5',
        'product_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Commission::class;
    }
}
