<?php

namespace App\Repositories\Admin;

use App\Models\Admin\Adviser;
use App\Repositories\BaseRepository;

/**
 * Class AdviserRepository
 * @package App\Repositories\Admin
 * @version January 26, 2021, 4:02 am UTC
*/

class AdviserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Adviser::class;
    }
}
