<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Repositories\ProductRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Product;
use App\Repositories\CommissionRepository;
use Illuminate\Http\Request;
use Flash;
use Inertia\Inertia;
use Response;

class ProductController extends AppBaseController
{
    /** @var  ProductRepository */
    private $productRepository;

    /** @var  CommissionRepository */
    private $commissionRepository;

    public function __construct(ProductRepository $productRepo, CommissionRepository $commissionRepo)
    {
        $this->productRepository = $productRepo;
        $this->commissionRepository = $commissionRepo;
    }

    /**
     * Display a listing of the Product.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $products = $this->productRepository->all();

        $products->load(['commissions']);

        return Inertia::render('Products/index', [
            'products' => $products
        ]);
    }

    /**
     * Show the form for creating a new Product.
     *
     * @return Response
     */
    public function create()
    {
        return Inertia::render('Products/create');
    }

    /**
     * Store a newly created Product in storage.
     *
     * @param CreateProductRequest $request
     *
     * @return Response
     */
    public function store(CreateProductRequest $request)
    {
        $input = $request->all();

        // Mover a repositorio
        $product = $this->productRepository->create($input);
        $product->commissions()->create($input['commissions']);

        return redirect()->back()
            ->with('message', 'Product Created Successfully.');
    }

    /**
     * Display the specified Product.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(Product $product)
    {
        return Inertia::render('Products/show', [
            'product' => $product
        ]);
    }

    /**
     * Show the form for editing the specified Product.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        return view('products.edit')->with('product', $product);
    }

    /**
     * Update the specified Product in storage.
     *
     * @param int $id
     * @param UpdateProductRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProductRequest $request)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $this->productRepository->update($request->all(), $id);

        $product->commissions->level0 = $request['commissions']['level0'];
        $product->commissions->level1 = $request['commissions']['level1'];
        $product->commissions->level2 = $request['commissions']['level2'];
        $product->commissions->level3 = $request['commissions']['level3'];
        $product->commissions->level4 = $request['commissions']['level4'];
        $product->commissions->level5 = $request['commissions']['level5'];

        $product->commissions->save();

        return redirect()->back()
            ->with('message', 'Post Updated Successfully.');
    }

    /**
     * Remove the specified Product from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $product = $this->productRepository->find($id);

        if (empty($product)) {
            Flash::error('Product not found');

            return redirect(route('products.index'));
        }

        $this->productRepository->delete($id);

        return redirect()->back()
            ->with('status', 'Adviser Updated Successfully.');
    }
}
