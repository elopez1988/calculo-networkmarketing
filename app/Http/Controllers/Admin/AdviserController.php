<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CreateAdviserRequest;
use App\Http\Requests\Admin\UpdateAdviserRequest;
use App\Repositories\Admin\AdviserRepository;
use App\Http\Controllers\AppBaseController;
use App\Imports\AdviserImport;
use App\Models\Admin\Adviser;
use Illuminate\Http\Request;
use Flash;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;
use Response;

class AdviserController extends AppBaseController
{
    /** @var  AdviserRepository */
    private $adviserRepository;

    public function __construct(AdviserRepository $adviserRepo)
    {
        $this->adviserRepository = $adviserRepo;
    }

    /**
     * Display a listing of the Adviser.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        return Inertia::render('Advisers/index', [
            'advisers' => $this->adviserRepository->all()
        ]);
    }

    /**
     * Show the form for creating a new Adviser.
     *
     * @return Response
     */
    public function create()
    {
        return Inertia::render('Advisers/create');
    }

    /**
     * Store a newly created Adviser in storage.
     *
     * @param CreateAdviserRequest $request
     *
     * @return Response
     */
    public function store(CreateAdviserRequest $request)
    {
        $input = $request->all();

        $this->adviserRepository->create($input);

        return redirect()->back()
            ->with('message', 'Post Created Successfully.');
    }

    /**
     * Display the specified Adviser.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(Adviser $adviser)
    {
        return Inertia::render('Advisers/show', [
            'adviser' => $adviser
        ]);
    }

    /**
     * Show the form for editing the specified Adviser.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $adviser = $this->adviserRepository->find($id);

        if (empty($adviser)) {
            Flash::error('Adviser not found');

            return redirect(route('admin.advisers.index'));
        }

        return view('admin.advisers.edit')->with('adviser', $adviser);
    }

    /**
     * Update the specified Adviser in storage.
     *
     * @param int $id
     * @param UpdateAdviserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAdviserRequest $request)
    {
        $adviser = $this->adviserRepository->find($id);

        if (empty($adviser)) {
            Flash::error('Adviser not found');

            return redirect(route('admin.advisers.index'));
        }

        $this->adviserRepository->update($request->all(), $id);

        return redirect()->back()
            ->with('message', 'Post Updated Successfully.');
    }

    /**
     * Remove the specified Adviser from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $adviser = $this->adviserRepository->find($id);

        if (empty($adviser)) {
            Flash::error('Adviser not found');

            return redirect(route('admin.advisers.index'));
        }

        $adviser->forceDelete();

        return redirect()->back()
            ->with('status', 'Adviser Updated Successfully.');
    }

    public function imports(Request $request)
    {
        Excel::import(new AdviserImport, $request->file('file'));
        return redirect()->back();
    }
}
