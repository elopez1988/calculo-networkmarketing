<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSaleRequest;
use App\Http\Requests\UpdateSaleRequest;
use App\Repositories\SaleRepository;
use App\Http\Controllers\AppBaseController;
use App\Imports\SaleImport;
use App\Models\Admin\Adviser;
use App\Models\Product;
use App\Models\Sale;
use Illuminate\Http\Request;
use Flash;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;
use Response;

class SaleController extends AppBaseController
{
    /** @var  SaleRepository */
    private $saleRepository;

    public function __construct(SaleRepository $saleRepo)
    {
        $this->saleRepository = $saleRepo;
    }

    /**
     * Display a listing of the Sale.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $sales = $this->saleRepository->all();

        $saledetails = [];
        foreach ($sales as $key => $sale) {

            $saledetails[] = $this->details($sale);
        }

        return Inertia::render('Sales/index', [
            'sales' => $saledetails
        ]);
    }

    /**
     * Detail Sale.
     *
     * @return Response
     */
    public function details($sale)
    {
        $count = count(str_split($sale->code, 2));
        $array = [];
        $length = strlen($sale->code);
        $code = $sale->code;

        for ($i = 0; $i < $length; $i += 2) {
            if (empty($array)) {
                $array[] = $code;
            } else {
                $array[] = substr($code,  0, -2);
                $code = substr($code,  0, -2);
            }
        }

        //Asesores
        $advisers = Adviser::whereIn('code', $array)->get();

        $sale->level = $count;
        $sale->advisers = $advisers;
        $sale->email = $advisers->last()->email;
        $sale->adviser = $advisers->last()->name;

        return $sale;
    }

    /**
     * Show the form for creating a new Sale.
     *
     * @return Response
     */
    public function create()
    {
        return view('sales.create');
    }

    /**
     * Store a newly created Sale in storage.
     *
     * @param CreateSaleRequest $request
     *
     * @return Response
     */
    public function store(CreateSaleRequest $request)
    {
        $input = $request->all();

        $sale = $this->saleRepository->create($input);

        Flash::success('Sale saved successfully.');

        return redirect(route('sales.index'));
    }

    /**
     * Display the specified Sale.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(Sale $sale)
    {

        if (empty($sale)) {
            Flash::error('Sale not found');

            return redirect(route('sales.index'));
        }

        $sale = $this->details($sale);

        //Comisiones
        $product = Product::where('name', $sale->product)->first();

        $key[1] = $product->commissions->level0;
        $key[2] = $product->commissions->level1;
        $key[3] = $product->commissions->level2;
        $key[4] = $product->commissions->level3;
        $key[5] = $product->commissions->level4;
        $key[6] = $product->commissions->level5;

        for ($i = 0; $i < $sale->level; $i++) {
            $b[] = $key[$i + 1];
        }
        $b = array_reverse($b);

        for ($i = 0; $i < $sale->level; $i++) {
            $sale->advisers[$i]['commission'] = $b[$i];
        }

        $sale->advisers = $sale->advisers->sortDesc();

        return Inertia::render('Sales/show', [
            'sale' => $sale
        ]);
    }

    /**
     * Show the form for editing the specified Sale.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $sale = $this->saleRepository->find($id);

        if (empty($sale)) {
            Flash::error('Sale not found');

            return redirect(route('sales.index'));
        }

        return view('sales.edit')->with('sale', $sale);
    }

    /**
     * Update the specified Sale in storage.
     *
     * @param int $id
     * @param UpdateSaleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSaleRequest $request)
    {
        $sale = $this->saleRepository->find($id);

        if (empty($sale)) {
            Flash::error('Sale not found');

            return redirect(route('sales.index'));
        }

        $sale = $this->saleRepository->update($request->all(), $id);

        Flash::success('Sale updated successfully.');

        return redirect(route('sales.index'));
    }

    /**
     * Remove the specified Sale from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $sale = $this->saleRepository->find($id);

        if (empty($sale)) {
            Flash::error('Sale not found');

            return redirect(route('sales.index'));
        }

        $this->saleRepository->delete($id);

        Flash::success('Sale deleted successfully.');

        return redirect(route('sales.index'));
    }

    public function imports(Request $request)
    {
        Excel::import(new SaleImport, $request->file('file'));
        return redirect()->back();
    }
}
