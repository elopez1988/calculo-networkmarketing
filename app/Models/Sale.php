<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Sale
 * @package App\Models
 * @version February 2, 2021, 8:44 pm UTC
 *
 * @property string $code
 * @property string $product
 * @property number $quantity
 * @property number $amount
 */
class Sale extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'sales';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'code',
        'product',
        'quantity',
        'amount',
        'payment_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'code' => 'string',
        'product' => 'string',
        'quantity' => 'float',
        'amount' => 'float',
        'payment_date' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'code' => 'required|string|max:255',
        'product' => 'required|string|max:255',
        'quantity' => 'required|numeric',
    ];
}
