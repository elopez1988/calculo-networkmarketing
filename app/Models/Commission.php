<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Commission
 * @package App\Models
 * @version February 4, 2021, 6:59 pm UTC
 *
 * @property \App\Models\Product $product
 * @property string $leve0
 * @property string $leve1
 * @property string $leve2
 * @property string $leve3
 * @property string $leve4
 * @property string $leve5
 * @property integer $product_id
 */
class Commission extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'commissions';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'level0',
        'level1',
        'level2',
        'level3',
        'level4',
        'level5',
        'product_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'level0' => 'string',
        'level1' => 'string',
        'level2' => 'string',
        'level3' => 'string',
        'level4' => 'string',
        'level5' => 'string',
        'product_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'level0' => 'required|string|max:255',
        'level1' => 'required|string|max:255',
        'level2' => 'required|string|max:255',
        'level3' => 'required|string|max:255',
        'level4' => 'required|string|max:255',
        'level5' => 'required|string|max:255',
        'product_id' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function product()
    {
        return $this->belongsTo(\App\Models\Product::class, 'product_id');
    }
}
