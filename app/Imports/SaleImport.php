<?php

namespace App\Imports;

use App\Models\Sale;
use Maatwebsite\Excel\Concerns\ToModel;

class SaleImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Sale([
            'code'     => $row[0],
            'product'  => $row[1],
            'quantity' => $row[2],
            // 'payment_date' => $row[3]
        ]);
    }
}
