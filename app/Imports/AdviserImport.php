<?php

namespace App\Imports;

use App\Models\Admin\Adviser;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithUpserts;

class AdviserImport implements ToModel, WithUpserts
{
    /**
     * @return string|array
     */
    public function uniqueBy()
    {
        return 'email';
    }

    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Adviser([
            'name'     => $row[0],
            'email'    => $row[1],
            'code'     => $row[2]
        ]);
    }
}
