<?php

namespace Database\Factories;

use App\Models\Commission;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommissionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Commission::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'level0' => $this->faker->word,
            'level1' => $this->faker->word,
            'level2' => $this->faker->word,
            'level3' => $this->faker->word,
            'level4' => $this->faker->word,
            'level5' => $this->faker->word,
            'product_id' => round(1, 2),
            'created_at' => $this->faker->date('Y-m-d H:i:s'),
            'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
